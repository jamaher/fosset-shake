//
//  Configuration.swift
//  The Fosset Shake
//
//  Created by Jason Maher on 7/29/15.
//  Copyright (c) 2015 Korokoro Studios. All rights reserved.
//

import Foundation



class Configuration {

    private static let OPTION_SETS_KEY = "kks.fosset.shake.option.sets"
    private static let OPTION_NAMES_KEY = "kks.fosset.shake.option.names"

    private static let ORIGINAL_NAMES = ["Movement", "Step", "Tengi", "Sotai", "Direction", "Attack"]
    private static let ORIGINAL_VALUES = [
        ["Unsoku", "Unshin", "Both"],
        ["So", "In", "Ka", "Gen", "Ko", "Ten", "Tsui", "Tai", "Hen-Ka", "Hen-Gen"],
        ["Zenten/Handspring", "Koten", "Bakuten", "Bakuchu", "Sokuten/Sokuchu"],
        ["Sen", "Un", "Hen", "Nen", "Ten"],
        ["Jun", "Gyaku", "Ushiro", "Tobi", "Noashi", "Fukuteki"],
        ["Punch", "Strike", "Kick", "Takedown", "Grab"]
    ]

    static let INSTANCE = Configuration()

    private let store: NSUserDefaults
    var options: [[String]]
    var names: [String]

    private init() {
        store = NSUserDefaults.standardUserDefaults()
        if let existing = store.arrayForKey(Configuration.OPTION_NAMES_KEY) as? [String] {
            names = existing
        } else {
            names = Configuration.ORIGINAL_NAMES
        }
        if let existing = store.arrayForKey(Configuration.OPTION_SETS_KEY) as? [[String]] {
            options = existing
        } else {
            options = Configuration.ORIGINAL_VALUES
        }
    }

    func reset() {
        names = Configuration.ORIGINAL_NAMES
        options = Configuration.ORIGINAL_VALUES
        save()
    }

    func groupName(group: Int) -> String {
        return names[group]
    }

    func optionGroup(group: Int) -> [String] {
        return options[group]
    }

    func optionGroup(group: String) -> [String] {
        let index = find(names, group)!
        return optionGroup(index)
    }

    func randomOption(group: Int) -> String {
        let group = optionGroup(group)
        if group.isEmpty {
            return "(empty)"
        }
        return sample(group)
    }

    func randomOption(group: String) -> String {
        let index = find(names, group)!
        return randomOption(index)
    }

    func option(path: NSIndexPath) -> String {
        return optionGroup(path.section)[path.row]
    }

    func append(path: NSIndexPath, option: String) {
        options[path.section].append(option)
        save()
    }

    func delete(path: NSIndexPath) {
        options[path.section].removeAtIndex(path.row)
        save()
    }

    func replace(path: NSIndexPath, option: String) {
        options[path.section][path.row] = option
        save()
    }

    func rename(section: Int, to value: String) {
        names[section] = value
        save()
    }

    func drop(section: Int) {
        options.removeAtIndex(section)
        names.removeAtIndex(section)
        save()
    }

    func createSection(name: String) {
        names.append(name)
        options.append([])
        save()
    }

    private func save() {
        store.setObject(options, forKey: Configuration.OPTION_SETS_KEY)
        store.setObject(names, forKey: Configuration.OPTION_NAMES_KEY)
    }

    func logGroup(group: Int) {
        let options = optionGroup(group)
        println("Group[\(group)] contains \(options.count) items: \(options)")
    }

}