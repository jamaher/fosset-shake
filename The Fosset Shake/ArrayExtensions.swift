//
//  ArrayExtensions.swift
//  MazerCLI
//
//  Created by Jason Maher on 3/9/15.
//  Copyright (c) 2015 Jason Maher. All rights reserved.
//

import Foundation

/// TODO: move all the top level functions back in here once we can have generic extensions
public extension Array {

}

/// Samples a random element from this array. Explodes if the array is empty.
///
/// :return: a random element from within the array.
public func sample<T>(array: [T]) -> T {
    return array[Random.int(0..<array.count)]
}

/// Returns a range from 0 to the limit of the array.
///
/// :return: a range over the given array.
public func range<T>(array: [T]) -> Range<Int> {
    return 0..<array.count
}