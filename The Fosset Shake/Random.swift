//
//  Random.swift
//  MazerCLI
//
//  Created by Jason Maher on 3/9/15.
//  Copyright (c) 2015 Jason Maher. All rights reserved.
//

import Foundation

/// A collection of functions for generating random values. For all functions that return `Int` the random
/// value will not be above `UInt32.max` since `arc4random_uniform()` is being used to generate the value.
/// Int is used as the return for convenience.
public struct Random {

    /// :return: a random Int between 0 and UInt32.max.
    public static func int() -> Int {
        return Int(arc4random_uniform(UInt32.max))
    }

    /// :return: a random Int in the range specified.
    public static func int(range: Range<Int>) -> Int {
        return Int(arc4random_uniform(UInt32(range.endIndex - range.startIndex))) + range.startIndex
    }

    /// :return: a random boolean.
    public static func coin() -> Bool {
        return int(0...1) == 0
    }

}

public struct Andyisms {

    static let isms: [String] = [
        "Do 50 of this and in the morning you'll se my face!",
        "Niiiiice",
        "I can do that, while inverted",
        "Do it on the other side!",
        "Stop slacking! why aren't you at 100 yet!",
        "Hawaii is like Kawaii man"
    ]

    static func random() -> String {
        return sample(isms)
    }

}