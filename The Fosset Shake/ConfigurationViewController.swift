//
//  SecondViewController.swift
//  The Fosset Shake
//
//  Created by Jason Maher on 7/29/15.
//  Copyright (c) 2015 Korokoro Studios. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    enum EditDeleteOption {
        case Edit
        case Delete
    }

    private static let NORMAL_CELL = "ShakeItCell"
    private static let ADD_OPTION_CELL = "ShakeItAddCell"
    private static let NOOP_CANCEL = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }

    let config = Configuration.INSTANCE

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var resetButton: UIBarButtonItem!

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        table.allowsMultipleSelectionDuringEditing = false
        resetButton.target = self
        resetButton.action = "resetOptions"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return config.names.count + 1
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (isAddSectionHeader(section)) {
            return "(Add new section)"
        } else {
            return config.groupName(section)
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // the extra row will be used to load the 'add new option' placehole cell
        if (isAddSectionHeader(section)) {
            return 0
        } else {
            return config.optionGroup(section).count + 1
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if isAddOptionRow(indexPath) {
            return addNewOptionCell(dequeCell(table, identifier: ConfigurationViewController.ADD_OPTION_CELL))
        } else {
            return displayOptionCell(indexPath, existing: dequeCell(table, identifier: ConfigurationViewController.NORMAL_CELL))
        }
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == .Delete) {
            config.delete(indexPath)
            table.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }

    // MARK: - UITableViewDelegate

    /// When an option row is selected pops up an action sheet for delete or edit. If an 'add' row is selected
    /// then jump directly to an input dialoh.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isAddOptionRow(indexPath) {
            showEditInputDialog("Create Option",
                message: "Enter a new option for the '\(config.groupName(indexPath.section))' group",
                content: nil,
                placeholder: "option") { (value) in
                    tableView.beginUpdates()
                    self.config.append(indexPath, option: value)
                    tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
                    tableView.endUpdates()
            }
        }
        else {
            showEditDeleteSheet { (choice) in
                if choice == .Edit {
                    self.showEditInputDialog("Edit Option",
                        message: "",
                        content: self.config.option(indexPath),
                        placeholder: nil) { (value) in
                            tableView.beginUpdates()
                            self.config.replace(indexPath, option: value)
                            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
                            tableView.endUpdates()
                    }
                } else {
                    tableView.beginUpdates()
                    self.config.delete(indexPath)
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    tableView.endUpdates()
                }
            }
        }
    }

    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            header.userInteractionEnabled = true
            let sectionTap = UITapGestureRecognizer(target: self, action: "sectionClicked:")
            header.tag = section
            header.addGestureRecognizer(sectionTap)
            let labelTap = UITapGestureRecognizer(target: self, action: "sectionClicked:")
            header.textLabel.tag = section
            header.addGestureRecognizer(labelTap)
        }
    }

    // MARK: - Instance methods

    func sectionClicked(gesture: UITapGestureRecognizer) {
        let section = gesture.view!.tag
        if (isAddSectionHeader(section)) {
            self.showEditInputDialog("Add Section",
                message: "",
                content: nil,
                placeholder: "section name") { (value) in
                    self.config.createSection(value)
                    self.table.reloadData()
            }
        } else {
            showEditDeleteSheet(title: "Edit Section", message: nil) { (option) in
                if (option == .Delete) {
                    self.config.drop(section)
                    self.table.reloadData()
                } else {
                    self.showEditInputDialog("Edit Section Name",
                        message: "",
                        content: self.config.groupName(section),
                        placeholder: nil) { (value) in
                            self.config.rename(section, to: value)
                            self.table.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
                    }
                }
            }
        }
    }

    func dequeCell(table: UITableView, identifier: String) -> UITableViewCell? {
        return table.dequeueReusableCellWithIdentifier(identifier) as? UITableViewCell
    }

    func displayOptionCell(path: NSIndexPath, existing: UITableViewCell?) -> UITableViewCell {
        let cell = (existing != nil) ? existing! : UITableViewCell(style: .Default, reuseIdentifier: ConfigurationViewController.NORMAL_CELL)
        cell.textLabel?.text = config.option(path)
        return cell
    }

    func addNewOptionCell(existing: UITableViewCell?) -> UITableViewCell {
        if existing != nil {
            return existing!
        }
        let cell = UITableViewCell(style: .Default, reuseIdentifier: ConfigurationViewController.ADD_OPTION_CELL)
        let button = UIButton.buttonWithType(UIButtonType.ContactAdd) as! UIButton
        button.userInteractionEnabled = false
        cell.accessoryView = button
        cell.textLabel?.text = "Add option..."
        cell.textLabel?.textColor = UIColor.grayColor()
        return cell
    }

    func isAddOptionRow(path: NSIndexPath) -> Bool {
        let group = config.optionGroup(path.section)
        // the fake cells we add at the bottom will == the count of options in a group
        return group.count == path.row
    }

    func isAddSectionHeader(section: Int) -> Bool {
        // same as `isAddOptionRow` but for headers instead of rows
        return config.names.count == section
    }

    /// Creates an alert dialog with a text field for editing a value. If content is provided then the field is
    /// prepopulated. If placeholder is provided then a placeholder is shown only if content is nill.
    func showEditInputDialog(title: String, message: String, content: String?, placeholder: String?, handler: (String) -> ()) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        controller.addTextFieldWithConfigurationHandler{ (field) in
            if let value = content {
                field.text = value
            } else if let value = placeholder {
                field.placeholder = content
            }
        }
        let saveAction = UIAlertAction(title: "Save", style: .Default) { (_) in
            let field = controller.textFields![0] as! UITextField
            let content = field.text
            if !content.isEmpty {
                handler(content)
            }
        }
        controller.addAction(ConfigurationViewController.NOOP_CANCEL)
        controller.addAction(saveAction)
        presentViewController(controller, animated: true, completion: nil)
    }

    /// Pops up an action sheet with generic [Edit, Delete, Cancel] buttons. The provided handler is only called
    /// if 'Delete' or 'Edit' is selected, cancel simply dismisses the dialog.
    func showEditDeleteSheet(title: String? = nil, message: String? = nil, handler: (EditDeleteOption) -> ()) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .ActionSheet)
        controller.addAction(ConfigurationViewController.NOOP_CANCEL)
        controller.addAction(UIAlertAction(title: "Edit", style: .Default) { (_) in handler(.Edit) })
        controller.addAction(UIAlertAction(title: "Delete", style: .Destructive) { (_) in handler(.Delete) })
        presentViewController(controller, animated: true, completion: nil)
    }

    func resetOptions() {
        config.reset()
        table.reloadData()
    }
}
