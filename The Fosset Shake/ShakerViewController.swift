//
//  FirstViewController.swift
//  The Fosset Shake
//
//  Created by Jason Maher on 7/29/15.
//  Copyright (c) 2015 Korokoro Studios. All rights reserved.
//

import UIKit

extension UIImageView {
    func vibrate(){
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 3
        shake.autoreverses = true
        shake.fromValue = NSValue(CGPoint: CGPointMake(self.center.x - 2.0, self.center.y))
        shake.toValue = NSValue(CGPoint: CGPointMake(self.center.x + 248.0, self.center.y))
        self.layer.addAnimation(shake, forKey: "position")
    }
}

class ShakerViewController: UIViewController {

    // determined from interface builder
    private static let ANCHOR_Y_OFFSET: CGFloat = 48

    let config = Configuration.INSTANCE
    var nameLabels: [UILabel] = []
    var optionLabels: [UILabel] = []

    @IBOutlet weak var nameAnchor: UILabel!
    @IBOutlet weak var optionAnchor: UILabel!
    @IBOutlet weak var andyLabel: UILabel!
    @IBOutlet weak var andyFace: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        nameAnchor.hidden = true
        optionAnchor.hidden = true
        manageLabels()
        randomizeLabels()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        manageLabels()
    }

    func manageLabels() {
        if nameLabels.count < config.names.count {
            for index in nameLabels.count..<config.names.count {
                let offset = ShakerViewController.ANCHOR_Y_OFFSET * CGFloat(index)

                let name = UILabel(frame: nameAnchor.frame)
                name.frame.origin.y = name.frame.origin.y + offset
                nameLabels.append(name)
                copyTextStyleFrom(nameAnchor, to: name)
                view.addSubview(name)
                alignInCenter(name)

                let option = UILabel(frame: optionAnchor.frame)
                option.frame.origin.y = option.frame.origin.y + offset
                optionLabels.append(option)
                copyTextStyleFrom(optionAnchor, to: option)
                view.addSubview(option)
                alignInCenter(option)
            }
        }
        while nameLabels.count > config.names.count {
            nameLabels.removeLast().removeFromSuperview()
            optionLabels.removeLast().removeFromSuperview()
        }
    }

    func copyTextStyleFrom(source: UILabel, to target: UILabel) {
        target.font = source.font
        target.textColor = source.textColor
        target.textAlignment = source.textAlignment
    }

    func alignInCenter(view: UIView) {
        view.frame.size.width = self.view.frame.width
    }

    func randomizeLabels() {
        for index in range(config.names) {
            nameLabels[index].text = "\(config.groupName(index)):"
            optionLabels[index].text = config.randomOption(index)
        }
        andyLabel.text = Andyisms.random()
        andyFace.vibrate()
    }

    @IBAction func touchedAndy(sender: AnyObject) {
        randomizeLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

